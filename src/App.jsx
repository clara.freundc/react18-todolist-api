import { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";
import TodoList from "./components/TodoList";
//on importe les éléments nécessaires (useEffect et axios)

const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  //useState pour ... du dynamisme?
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    //.get permet d'aller chercher les données (API_URL) et .then de les récupérer
    axios.get(API_URL).then((response) => {
      setTodos(response.data);
    });
  }, []);

  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
